import Chai, { expect } from 'chai'
import { AdminDashboard } from '../../../pages/index'
import promised from 'chai-as-promised'

Chai.use(promised);

describe('Add Charity', () => {
  //-------------------------------------------------------------------------
  // Page Objects
  //-------------------------------------------------------------------------

  var Charity         = AdminDashboard.Charity;
  var CharityAdd = Charity.CharityAdd;


  //-------------------------------------------------------------------------
  // Variables
  //-------------------------------------------------------------------------

  var charityName = protractor.helper.generate.uniqueValue('CharityName');

  //-------------------------------------------------------------------------
  // Tests
  //-------------------------------------------------------------------------
  describe('We2o Admin', () => {
    beforeEach(function () {
      browser.sleep(400);
    });

    it('should login', () => {
      HomePage.open();
      HomePage.logoutUser();
      HomePage.mainMenu.btnMyWe2o.click();
      SignInPage.signIn('pre-setup ' + browser.params.admin.username, 'pre-setup ' + browser.params.admin.password);
      browser.waitForAngular();
      expect(HomePage.getUsername()).to.eventually.equal(browser.params.admin.displayName);
    });

    it('should open charity modal', () => {
      Charity.open();
      Charity.openAddCharityModal();
      expect(modalAddCharity.txtModalAddCharityName.isEnabled()).to.eventually.equal(true);
    });

    it('should fill the form and submit', () => {
      expect(CharityAdd.btnModalAddCharitynDone.isEnabled()).to.eventually.equal(false);
      CharityAdd.setCharityName(charityName);
      CharityAdd.setCharityDescription(browser.params.newCharity.description);
      CharityAdd.setCharityMission(browser.params.newCharity.mission);
      expect(CharityAdd.btnModalAddCharitynDone.isEnabled()).to.eventually.equal(true);
      CharityAdd.charityDone();
      browser.waitForAngular();
    });

    it('should see newly created charity record in the charity grid', () => {
      Charity.searchForCharity(charityName);
      browser.waitForAngular();
      expect(Charity.tblCharityNames.first().getText()).to.eventually.equal(charityName);
      expect(Charity.tblCharityDescriptions.first().getText()).to.eventually.equal(browser.params.newCharity.description);
    });
  });
});