export default class CharityEdit {
  constructor() {
    this.txtCharityName = element(by.name('charity_name'));
    this.txtCharityDescription = element(by.name('charity_description'));
    this.txtCharityMission = element(by.name('charity_mission'));
    this.selCharityNavigatorStars = element(by.css('select[ng-model="charity.navigator_stars"]'));
    this.btnCharityPreview = element(by.css('button[ng-click="openPreviewMode(charity)"]'));
    this.btnCharitySave = element(by.css('button[ng-click="saveCharity(charity)"]'));
    this.btnCharityReturnToEdit = element(by.css('a[ng-click="returnToEditMode(charity)"]'));
  }

  /**
   * @param {string} charityName
   */
  setCharityName(charityName) {
    this.txtCharityName.clear();
    this.txtCharityName.sendKeys(charityName);
  };

  /**
   * @param {string} charityDescription
   */
  setCharityDescription(charityDescription) {
    this.txtCharityDescription.clear();
    this.txtCharityDescription.sendKeys(charityDescription);
  };

  /**
   * @param {string} charityMission
   */
  setCharityMission(charityMission) {
    this.txtCharityMission.clear();
    this.txtCharityMission.sendKeys(charityMission);
  };

  /**
   * @param {string} charityStars
   */
  setCharityNavigatorStars(charityStars) {
    protractor.helper.actions.moveMouseOver(this.selCharityNavigatorStars);
    protractor.helper.actions.selectFromDropdown(this.selCharityNavigatorStars, charityStars);
  };

  getCharityName() {
    protractor.helper.actions.waitUntilElementIsVisible(this.txtCharityName);
    return this.txtCharityName.getAttribute('value').then(result => result);
  }

  getCharityDescription() {
    protractor.helper.actions.waitUntilElementIsVisible(this.txtCharityDescription);
    return this.txtCharityDescription.getAttribute('value').then(result => result);
  }

  getCharityMission() {
    protractor.helper.actions.waitUntilElementIsVisible(this.txtCharityMission);
    return this.txtCharityMission.getAttribute('value').then(result => result);
  }

  previewCharity() {
    protractor.helper.actions.moveMouseOver(this.btnCharityPreview);
    this.btnCharityPreview.click();
    browser.waitForAngular();
  };

  saveCharity() {
    protractor.helper.actions.moveMouseOver(this.btnCharitySave);
    this.btnCharitySave.click();
    browser.waitForAngular();
  };

  returnToCharity() {
    protractor.helper.actions.moveMouseOver(this.btnCharityReturnToEdit);
    this.btnCharityReturnToEdit.click();
    browser.waitForAngular();
  };
}