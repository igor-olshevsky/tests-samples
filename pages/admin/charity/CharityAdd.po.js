export default class CharityAdd {
  constructor() {
    this.txtModalAddCharityName = element(by.id('charity-name'));
    this.txtModalAddCharityDescription = element(by.id('description'));
    this.txtModalAddCharityMission = element(by.id('mission'));
    this.btnModalAddCharitynDone = element(by.css('button[ng-click="createCharity(charity)"]'));
  }

  /**
   * @param {string} charityName
   */
  setCharityName(charityName) {
    this.txtModalAddCharityName.sendKeys(charityName);
  }

  /**
   * @param {string} charityDescription
   */
  setCharityDescription(charityDescription) {
    this.txtModalAddCharityDescription.sendKeys(charityDescription);
  }

  /**
   * @param {string} charityMission
   */
  setCharityMission(charityMission) {
    this.txtModalAddCharityMission.sendKeys(charityMission);
  };

  charityDone() {
    this.btnModalAddCharitynDone.click();
    browser.sleep(2000);
  };
}