import CharityAdd from './CharityAdd.po'
import CharityEdit from './CharityEdit.po'

export default class CharityPage {
  constructor() {
    this.btnAddCharity = element(by.css('button[ng-click="openNewCharityModal()"]'));
    this.btnOpenSearch = element(by.css('div[ng-click="toggleShowMenu()"]'));
    this.txtSearch = element(by.css('input[placeholder="Search..."]'));
    this.tblCharityNames = element.all(by.css('[ng-repeat="row in renderedRows"] .col0.colt0 span'));
    this.tblCharityDescriptions = element.all(by.css('[ng-repeat="row in renderedRows"] .col1.colt1 span'));
    this.tblCharityEdits = element.all(by.css('[ng-repeat="row in renderedRows"] .col8.colt8 span'));

    this.CharityAdd = new CharityAdd();
    this.CharityEdit = new CharityEdit();
  }

  open() {
    browser.get('/admin/charity');
  };

  openAddCharityModal() {
    this.btnAddCharity.click();
    browser.waitForAngular();
  };

  /**
   * @param {string} charityName
   */
  searchForCharity (charityName) {
    this.btnOpenSearch.click();
    this.txtSearch.sendKeys(charityName);
    browser.waitForAngular();
    this.btnOpenSearch.click();
  }

  editCharity() {
    this.tblCharityEdits.first().click();
    browser.waitForAngular();
  }
}